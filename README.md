# BBC Microbit Neural Network

Originally published 4 Oct 2019

A neural network designed to run on a [BBC Microbit](https://www.microbit.org/)

## Neural Network Task

The neural network is fed 4 sets of 5 numbers, either a 0 or a 1. These numbers are represented by on or off lights on the first 4 rows of the Microbit's display. Whilst learning, the Microbit processes each set of numbers and returns a number between 0 and 1. This number should be the first number in the set, and the neural network will hopefully pick that up during it's learning phase. The Microbit runs 1000 learning itterations before being fed a new set of 5 numbers, represented by the lights on the bottom row of the Microbit's display and printing it's output in scrolling text on the display. This should be as close to the status of the first light in the set as possible.

## Usage

* Go to the Microbit's [online python editor](https://python.microbit.org/v/2) and load either `neuralnetwork.py` or `neuralnetwork.hex`, then save the code to your Microbit
* Once the Microbit's code has been saved to it and it is powered on, hold the A button to generate a random data set. You should aim to have 2 out of the 4 first rows start with an on light, and 2 of them start with an off light in order to have varied training data
* Once you are happy with your data set, press the B button to begin training. The status of it's training phase is represented by a loading bar of pixels which will fill up the screen once it has finished training
* Obsereve the output of the testing data as it scrolls across the screen

## Video Demonstration

[YouTube](https://www.youtube.com/watch?v=6LgzcYY7GHk)
